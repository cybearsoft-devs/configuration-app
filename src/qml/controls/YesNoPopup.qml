import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Popup {
    id: popup
    modal: true
    width: 350
    height: 150

    background: Rectangle {
        color: "transparent"
    }

    property var onClickedYes: function() {}
    property string text: "Are you sure you want to save config?"

    contentItem: Rectangle {
        color: "#F3F3F3"
        radius: 4

        Text {
            id: messageText

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                topMargin: 25
            }

            text: popup.text
            color: "black"
            font {
                family: "Inter"
                weight: 400
                pixelSize: 14
            }
        }

        Row {
            id: buttonRow
            spacing: 16

            anchors {
                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                bottomMargin: 16
            }

            CustomButton {
                id: yesBtn
                text: qsTr("Yes")

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignLeft

                onClicked: {
                    popup.onClickedYes()
                    popup.close()
                }
            }

            CustomButton {
                id: noBtn
                text: qsTr("No")

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignRight

                onClicked: {
                    popup.close()
                }
            }
        }
    }
}