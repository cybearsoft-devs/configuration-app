import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15


Rectangle {
    id: paramsTable
    color: "transparent"
    clip: true
    radius: 4

    property int columnXPadding: 64

    property int firstVSeparatorX: 750
    property int minimumFirstVSeparatorX: paramsTable.width - secondVSeparator.x + valueLabel.width + columnXPadding
    property int maximumFirstVSeparatorX: paramsTable.width - parameterLabel.width - columnXPadding - 24

    property int secondVSeparatorX: 390
    property int minimumSecondVSeparatorX: newValueLabel.width + columnXPadding
    property int maximumSecondVSeparatorX: paramsTable.width - firstVSeparator.x - valueLabel.width - columnXPadding - 50

    property var paramsModel: []
    property var onClickedCopy: function() {}

    border {
        color: "#5A5A5A"
        width: 1
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            forceActiveFocus()
            mainWindow.hideTooltips()
        }
    }

    anchors {
        top: parent.top
        bottom: parent.bottom
        right: parent.right
        rightMargin: 120
        bottomMargin: 31
        topMargin: 80
        fill: parent
    }

    RowLayout {

        anchors {
            left: parent.left
            right: firstVSeparator.left
            bottom: hSeparator.top
            bottomMargin: 8
        }

        Label {
            id: parameterLabel
            text: qsTr("Parameter")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 700
                pixelSize: 16
            }

            Layout.alignment: Qt.AlignHCenter
        }
    }

    RowLayout {

        anchors {
            left: firstVSeparator.right
            right: secondVSeparator.left
            bottom: hSeparator.top
            bottomMargin: 8
        }

        Label {
            id: valueLabel
            text: qsTr("Machine Values")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 700
                pixelSize: 16
            }

            Layout.alignment: Qt.AlignHCenter
        }
    }

    RowLayout {

        anchors {
            left: secondVSeparator.right
            right: parent.right
            bottom: hSeparator.top
            bottomMargin: 8
        }

        Label {
            id: newValueLabel
            text: qsTr("New Values")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 700
                pixelSize: 16
            }

            Layout.alignment: Qt.AlignHCenter
        }
    }

    ToolSeparator {
        id: hSeparator
        width: parent.width
        orientation: Qt.Horizontal

        contentItem: Rectangle {
            implicitHeight: 1
            color: "#5A5A5A"
        }

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            topMargin: 48
            leftMargin: -4
            rightMargin: -4
        }
    }

    ToolSeparator {
        id: firstVSeparator
        height: parent.height
        orientation: Qt.Vertical

        contentItem: Rectangle {
            implicitWidth: 1
            color: "#5A5A5A"
        }

        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
            topMargin: -4
            rightMargin: Math.min(paramsTable.firstVSeparatorX, paramsTable.maximumFirstVSeparatorX)
            bottomMargin: -4
        }

        MouseArea {
            id: firstVSeparatorMouseArea
            width: parent.width
            height: 60
            hoverEnabled: true
            cursorShape: Qt.SizeHorCursor

            property bool isPressed: false

            onPressed: {
                firstVSeparatorMouseArea.isPressed = true
            }

            onReleased: {
                if (firstVSeparatorMouseArea.isPressed) {
                    firstVSeparatorMouseArea.isPressed = false
                }
            }

            onPositionChanged: {
                if (firstVSeparatorMouseArea.isPressed) {
                    var newX = paramsTable.firstVSeparatorX - (mouseX - firstVSeparatorMouseArea.width / 2)
                    newX = Math.max(paramsTable.minimumFirstVSeparatorX, Math.min(paramsTable.maximumFirstVSeparatorX, newX))
                    paramsTable.firstVSeparatorX = newX
                }
            }
        }
    }

    ToolSeparator {
        id: secondVSeparator
        height: parent.height
        orientation: Qt.Vertical

        contentItem: Rectangle {
            implicitWidth: 1
            color: "#5A5A5A"
        }

        anchors {
            top: parent.top
            right: parent.right
            bottom: parent.bottom
            topMargin: -4
            rightMargin: Math.min(paramsTable.secondVSeparatorX, (paramsTable.maximumFirstVSeparatorX - valueLabel.width - paramsTable.columnXPadding - 24))
            bottomMargin: -4
        }

        MouseArea {
            id: secondVSeparatorMouseArea
            width: parent.width
            height: 60
            hoverEnabled: true
            cursorShape: Qt.SizeHorCursor

            property bool isPressed: false

            onPressed: {
                secondVSeparatorMouseArea.isPressed = true
            }

            onReleased: {
                if (secondVSeparatorMouseArea.isPressed) {
                    secondVSeparatorMouseArea.isPressed = false
                }
            }

            onPositionChanged: {
                if (secondVSeparatorMouseArea.isPressed) {
                    var newX = paramsTable.secondVSeparatorX - (mouseX - secondVSeparatorMouseArea.width / 2)
                    newX = Math.max(paramsTable.minimumSecondVSeparatorX, Math.min(paramsTable.maximumSecondVSeparatorX, newX))
                    paramsTable.secondVSeparatorX = newX
                }
            }
        }
    }

    CustomButton {
        id: copyBtn
        text: qsTr(">")

        anchors {
            top: secondVSeparator.top
            right: secondVSeparator.left
            topMargin: 16
            rightMargin: -24
        }

        enabledColor: "#5A5A5A"

        btnWidth: 10
        btnHeight: 2
        btnRadius: 4

        font {
            family: "Inter"
            weight: 600
            pixelSize: 10
        }

        Layout.alignment: Qt.AlignVCenter

        onClicked: {
            paramsTable.onClickedCopy()
        }
    }

    ParamsView {
        id: paramsView

        secondColumLeftSideX: Math.min(paramsTable.firstVSeparatorX, paramsTable.maximumFirstVSeparatorX)
        secondColumRightSideX: Math.min(paramsTable.secondVSeparatorX, (paramsTable.maximumFirstVSeparatorX - valueLabel.width - paramsTable.columnXPadding - 24))

        model: paramsTable.paramsModel

        anchors {
            topMargin: 73.5
            leftMargin: 16
            rightMargin: 8
            bottomMargin: 9.5
            fill: parent
        }
    }
}