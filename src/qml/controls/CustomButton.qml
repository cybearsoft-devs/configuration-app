import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Button {
    id: control

    topInset: 0
    bottomInset: 0

    property string disabledColor: "#797979"
    property string hoveredColor: "#64D1FF"
    property string enabledColor: "#204C81"

    property var btnWidth: 160
    property var btnHeight: 48
    property var btnRadius: 30

    property string backgroundColor: {
        if (!control.enabled) {
            return disabledColor
        }
        else if (control.hovered) {
            return hoveredColor
        }
        else {
            return enabledColor
        }
    }

    contentItem: Text {
        text: control.text
        font: control.font
        color: "#F3F3F3"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        elide: Text.ElideRight

        style: Text.Normal
    }

    background: Rectangle {
        implicitWidth: btnWidth
        implicitHeight: btnHeight
        color: backgroundColor
        border.color: backgroundColor
        border.width: 1
        radius: btnRadius
    }
}