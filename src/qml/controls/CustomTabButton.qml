import QtQuick 2.15
import QtQuick.Controls 2.15

TabButton {
    id: btn

    property color enabledColor:  "#F3F3F3"
    property color disabledColor: "#797979"
    property color selectedColor: "#64D1FF"

    contentItem: Text {
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter

        text: btn.text
        style: Text.Normal
        color: {
            if (control.currentIndex === btn.index ) {
                return btn.selectedColor
            }
            else if (btn.enabled) {
                return btn.enabledColor
            }
            else {
                return btn.disabledColor
            }
        }

        font {
            family: "Inter"
            weight: 700
            pixelSize: 24
        }
    }

    background: Rectangle {
        implicitHeight: 48
        color: "transparent"
    }

    onClicked: {
        mainWindow.hideTooltips()
    }
}
