import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

ToolTip {
    id: tooltip
    parent: Overlay.overlay
    y: Overlay.overlay.height - height
    x: -(Overlay.overlay.width - width - 8)
    margins: 0

    Behavior on visible {
        NumberAnimation {
            duration: 0
        }
    }

    Connections {
        target: mainWindow
        ignoreUnknownSignals: true
        function onHideTooltips() {
            tooltip.hide()
        }
    }
}