import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

CheckBox {
    id: control
    checked: false
    spacing: 19

    Layout.alignment: Qt.AlignVCenter

    property string disabledTextColor: "#797979"
    property string enabledTextColor: "#F3F3F3"

    indicator: Rectangle {
        implicitWidth: 18
        implicitHeight: 18
        x: control.leftPadding
        y: parent.height / 2 - height / 2
        radius: 2
        color: "transparent"
        border.color: control.enabled ? enabledTextColor : disabledTextColor
        border.width: 2

        Image {
            id: nvIcon
            anchors.centerIn: parent
            source: "../../../resources/themes/material/icons/checked_icon.svg"
            visible: control.checked
        }
    }

    contentItem: Text {
        text: control.text
        color: control.enabled ? enabledTextColor : disabledTextColor
        font {
            family: "Inter"
            weight: 400
            pixelSize: 14
        }

        leftPadding: control.indicator.width + control.spacing
        verticalAlignment: Text.AlignVCenter
    }
}