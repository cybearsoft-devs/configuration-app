import QtQuick 2.15
import QtQuick.Controls 2.15

Item {
    property var intValidator: IntValidator {

    }
    property var doubleValidator: RegularExpressionValidator {
        regularExpression: /^[0-9]*(\.[0-9]*)?$/
    }
    property var stringValidator: RegularExpressionValidator {
        regularExpression: /^.*$/
    }
    property var boolValidator: RegularExpressionValidator {
        regularExpression: /^(True|False)$/
    }
    property var listValidator: RegularExpressionValidator {
        regularExpression: /^\[[^\]]*(,\s*[^\]]*)*\]$/
    }

    function getValidator(type) {
        if (type === "int") {
            return intValidator
        } else if (type === "float") {
            return doubleValidator
        } else if (type === "str") {
            return stringValidator
        } else if (type === "bool") {
            return boolValidator
        } else if (type === "list") {
            return listValidator
        }
        return stringValidator
    }
}
