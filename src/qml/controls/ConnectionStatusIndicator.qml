import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Rectangle {
    id: indicator
    width: 72
    height: 24
    radius: 20
    color: indicatorsList.currentIndex === indicator.index ? indicator.activeColor : "transparent"

    property string text: ""
    property string activeColor: ""
    property var index: undefined

    border {
        width: 1
        color: indicatorsList.currentIndex === indicator.index ? indicator.activeColor : "#959595"
    }

    Label {
        text: indicator.text
        color: indicatorsList.currentIndex === indicator.index ? "#0F0F0F" : "#959595"
        style: Text.Normal

        font {
            family: "Inter"
            weight: indicatorsList.currentIndex === indicator.index ? 600 : 400
            pixelSize: 12
        }

        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: parent.verticalCenter
        }
    }
}