import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

TabBar {
    id: control
    z: 2

    Material.accent: "#64D1FF"

    background: Rectangle {
        color: "transparent"
    }

    contentItem: ListView {
        model: control.contentModel
        currentIndex: control.currentIndex

        spacing: control.spacing
        orientation: ListView.Horizontal
        boundsBehavior: Flickable.StopAtBounds
        flickableDirection: Flickable.AutoFlickIfNeeded
        snapMode: ListView.SnapToItem

        highlightMoveDuration: 250
        highlightResizeDuration: 250
        highlightFollowsCurrentItem: true
        highlightRangeMode: ListView.ApplyRange

        highlight: Item {
            z: 2
            Rectangle {
                height: 4
                radius: 24
                width: parent.width
                y: parent.height - height + 5
                color: control.Material.accentColor
            }
        }
    }

    CustomTabButton {
        readonly property int index: 0

        text: qsTr("Station")
        width: 90

        enabled: true
    }

    CustomTabButton {
        readonly property int index: 1

        text: qsTr("Configuration")
        width: 167

        enabled: mainWindow.loadIsDone
    }

    CustomTabButton {
        readonly property int index: 2

        text: qsTr("Evaluation")
        width: 127

        enabled: mainWindow.loadIsDone
    }
}
