import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Rectangle {
    id: control
    width: 447
    height: 40
    clip: true
    radius: 8
    color: "transparent"
    border.color: "#959595"

    property string text: ""
    property color textColor: "#959595"
    property alias font: control.fontMetrics.font
    property FontMetrics fontMetrics: FontMetrics {
        font.pixelSize: 14
    }

    Label {
        id: label
        text: control.text
        width: control.width
        color: control.textColor
        style: Text.Normal
        font: control.font
        elide: Text.ElideRight

        verticalAlignment: Text.AlignVCenter

        anchors {
            verticalCenter: parent.verticalCenter
            right: parent.right
            left: parent.left
            rightMargin: 16 + folderRect.width
            leftMargin: 16
        }

        CustomToolTip {
            id: tooltip
        }

        MouseArea {
            id: labelMouseArea
            anchors.fill: parent
            hoverEnabled: true

            onEntered: {
                if (label.truncated) {
                    tooltip.show(label.text)
                }
            }
            onExited: {
                tooltip.hide()
            }
        }
    }

    Rectangle {
        id: folderRect
        implicitWidth: 26
        implicitHeight: 24
        radius: 4
        color: iconMouse.containsMouse ? Qt.rgba(0, 0, 0, 0.2) : "transparent"
        clip: true

        anchors {
            right: parent.right
            rightMargin: 13.33
            verticalCenter: parent.verticalCenter
        }

        Image {
            id: nvIcon
            anchors.centerIn: parent
            source: "../../../resources/themes/material/icons/folder_icon.svg"
        }

        MouseArea {
            id: iconMouse
            anchors.fill: parent
            hoverEnabled: true

            onClicked: {
                folderDialog.open()
            }
        }
    }

    FolderDialog {
        id: folderDialog

        onAccepted: {
            let path = currentFolder.toString()
            path = path.replace(/^(file:\/{3})/,"/")
            const cleanPath = decodeURIComponent(path)

            control.text = cleanPath
        }
    }
}