import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

TreeView {
    id: treeView
    rowHeight: 24
    hoverEnabled: true

    property int secondColumLeftSideX: 0
    property int secondColumRightSideX: 0

    font {
        family: "Inter"
        weight: 400
        pixelSize: 14
    }

    handle: Item {
        width: 20
        height: 20
        Rectangle {
            anchors.centerIn: parent
            width: 10
            height: 2
            color: currentRow.isHoveredIndex ? "black" : "#F3F3F3"
            visible: currentRow.hasChildren

            Rectangle {
                anchors.centerIn: parent
                width: parent.height
                height: parent.width
                color: parent.color
                visible: parent.visible && !currentRow.expanded
            }
        }
    }
}
