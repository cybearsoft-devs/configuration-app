import QtQuick 2.15
import QtQuick.Controls 2.15

Popup {
    id: popup

    property string icon: undefined
    property string backgroundColor: undefined
    property string text: ""

    padding: 0
    clip: true

    closePolicy: Popup.NoAutoClose

    background: Rectangle {
        implicitWidth: 449
        implicitHeight: 80
        color: popup.backgroundColor
        border.color: popup.backgroundColor
        radius: 4
    }

    contentItem: Rectangle {
        color: "transparent"

        Image {
            id: icon
            source: popup.icon
            width: 48
            height: 48
            sourceSize.width: 48
            sourceSize.height: 48

            anchors {
                top: parent.top
                left: parent.left
                topMargin: 16
                leftMargin: 16
            }
        }

        Label {
            id: stationBtnLabel
            text: popup.text
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 600
                pixelSize: 16
            }

            anchors {
                left: icon.right
                leftMargin: 24
                verticalCenter: parent.verticalCenter
            }
        }

        Image {
            id: closeIcon

            property string iconName: closeIconMouseArea.containsMouse ? "close_icon_hovered" :
                                                                         "close_icon"

            source: qsTr("../../../resources/themes/material/icons/%1.svg").arg(closeIcon.iconName)
            width: 32
            height: 32
            sourceSize.width: 32
            sourceSize.height: 32

            anchors {
                top: parent.top
                right: parent.right
                topMargin: 8
                rightMargin: 8
            }

            MouseArea {
                id: closeIconMouseArea
                anchors.fill: parent
                hoverEnabled: true

                onClicked: popup.close()
            }
        }
    }

    exit: Transition {
        NumberAnimation { property: "opacity"; from: 1.0; to: 0.0 }
    }

    Timer {
        id: autoCloseTimer
        interval: 1500
        repeat: false
        onTriggered: {
            popup.close()
        }
    }

    onOpened: {
        autoCloseTimer.restart()
    }

    onClosed: {
        autoCloseTimer.stop()
    }
}