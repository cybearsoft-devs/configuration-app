import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15


Rectangle {
    id: dropDown

    property int defaultHeight: 48
    property int defaultWidth: 185

    property var selectedOption: options.length === 1 ? options[0] : null
    property var selectedIndex: options.length === 1 ? 0 : null
    property var options: undefined
    property string dropDownName: ""

    signal optionUpdated()

    property string backgroundColor: {
        if (!dropDown.enabled) {
            return "#797979"
        } else if (selectedOption) {
            return "#204C81"
        } else {
            return "#434343"
        }
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        preventStealing: true
    }

    height: {
        if (dropDown.focus) {
            const optionsLength = dropDown.options.length
            const extHeight = (dropDown.defaultHeight * (optionsLength + 1))
            return optionsLength > 1 ? extHeight + 8 : extHeight
        } else {
            return dropDown.defaultHeight
        }
    }
    width: {
        if (dropDown.focus) {
            const newWidth = dropDown.getLongestLabelWidth(repeat) + 97.23
            return Math.max(newWidth, dropDown.defaultWidth)
        }
        return dropDown.defaultWidth
    }

    radius: 25
    z: dropDown.focus ? 2 : 1
    clip: true
    color: dropDown.backgroundColor

    Behavior on height {
        PropertyAnimation {
            duration: 150
        }
    }

    Behavior on z {
        PropertyAnimation {
            duration: 150
        }
    }

    Behavior on width {
        PropertyAnimation {
            duration: 150
        }
    }

    function getLongestLabelWidth(repeater) {
        var longestWidth = 0;
        for (var i = 0; i < repeater.count; i++) {
            var item = repeater.itemAt(i);
            var labelWidth = item.labelWidth;
            if (labelWidth > longestWidth) {
                longestWidth = labelWidth;
            }
        }
        return longestWidth > dropDownLabel.contentWidth ? longestWidth : dropDownLabel.contentWidth;
    }

    Rectangle {
        id: dropDownRect
        width: parent.width
        height: dropDown.defaultHeight

        color: "transparent"

        Label {
            id: dropDownLabel
            text: !dropDown.selectedOption ? dropDown.dropDownName : dropDown.selectedOption?.name ||
                                                                     dropDown.selectedOption
            color: "#F3F3F3"
            style: Text.Normal
            clip: true
            elide: Text.ElideRight
            width: parent.width - dropDownIndicator.width - 24 - 17.05

            font {
                family: "Inter"
                weight: 400
                pixelSize: 14
            }

            anchors {
                top: parent.top
                left: parent.left
                topMargin: 15.5
                leftMargin: 24
            }
        }

        Image {
            id: dropDownIndicator

            property string icon: {
                let downIcon = "drop_down_button_icon"
                let upIcon   = "drop_down_button_icon_up"

                if (dropDown.focus) {
                    return dropDownRectMouse.containsMouse ? upIcon + "_hovered" : upIcon
                }
                return dropDownRectMouse.containsMouse && dropDown.enabled  ? downIcon + "_hovered" : downIcon
            }

            source: qsTr("../../../../resources/themes/material/icons/%1.svg").arg(dropDownIndicator.icon)
            height: 30
            width: 30
            visible: !(options.length === 1)

            anchors {
                top: parent.top
                right: parent.right
                topMargin: 9
                rightMargin: 17.05
            }
        }

        MouseArea {
            id: dropDownRectMouse
            anchors.fill: parent
            hoverEnabled: true

            onClicked: {
                if (dropDown.options.length > 0 && !(dropDown.options.length === 1)) {
                    dropDown.focus = dropDown.focus ? 0 : 1
                }
            }
        }
    }

    Column {
        id: dropDownColumn
        width: parent.width
        spacing: 5

        anchors {
            top: dropDownRect.top
            topMargin: 46
        }

        Repeater {
            id: repeat
            model: dropDown.options

            Rectangle {
                id: dropDownItem

                property alias labelWidth: filterLabel.contentWidth

                width: parent.width - 44
                height: dropDown.defaultHeight
                color: "transparent"
                clip: true

                anchors {
                    left: parent.left
                    leftMargin: 14
                }

                Label {
                    id: filterLabel
                    width: parent.width
                    text: modelData?.name || modelData
                    color: dropDownItemMouse.containsMouse ? "#64D1FF" : "#F3F3F3"
                    style: Text.Normal

                    font {
                        family: "Inter"
                        weight: 400
                        pixelSize: 14
                    }

                    anchors {
                        top: parent.top
                        left: parent.left
                        topMargin: 10.5
                        leftMargin: 10
                    }
                }

                MouseArea {
                    id: dropDownItemMouse
                    anchors.centerIn: parent
                    width: parent.width
                    height: parent.height - 10
                    hoverEnabled: true

                    onClicked: {
                        dropDown.selectedOption = modelData
                        dropDown.selectedIndex = index
                        dropDown.optionUpdated()
                        dropDown.focus = 0
                    }
                }
            }
        }
    }
}