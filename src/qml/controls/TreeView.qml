import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15

Flickable {
    id: root

    implicitWidth: 400
    implicitHeight: 400
    clip: true

    property var model
    readonly property alias currentIndex: tree.selectedIndex
    readonly property alias currentItem: tree.currentItem
    property var currentData

    property alias handle: tree.handle
    property alias itemName: tree.itemName
    property alias itemValue: tree.itemValue
    property Component highlight: Rectangle {
        color: root.selectedColor
    }

    property alias selectionEnabled: tree.selectionEnabled
    property alias hoverEnabled: tree.hoverEnabled

    property alias color: tree.color
    property alias handleColor: tree.handleColor
    property alias hoverColor: tree.hoverColor
    property alias selectedColor: tree.selectedColor
    property alias selectedItemColor: tree.selectedItemColor

    property alias rowHeight: tree.rowHeight
    property alias rowPadding: tree.rowPadding
    property alias rowSpacing: tree.rowSpacing

    property alias fontMetrics: tree.fontMetrics
    property alias font: tree.font

    contentHeight: tree.height
    contentWidth: width
    boundsBehavior: Flickable.StopAtBounds
    ScrollBar.vertical: ScrollBar {

        contentItem: Rectangle {
            color: "#959595"
            implicitWidth: 6
            implicitHeight: 100
            radius: width / 2
        }

        background: Rectangle {
            radius: 4
            clip: true
            color: "transparent"
        }

        Behavior on opacity {
            NumberAnimation {}
        }
    }


    Connections {
        function onCurrentIndexChanged() {
            if (currentIndex) {
                currentData = model.data(currentIndex);
            }
        }
    }

    onMovementStarted: {
        mainWindow.hideTooltips()
    }

    TreeViewItem {
        id: tree

        model: root.model
        parentIndex: root.model?.rootIndex() || undefined
        childCount: root.model?.rowCount(parentIndex) || undefined

        itemLeftPadding: 0
        color: root.color
        hoverColor: root.hoverColor
        selectedColor: root.selectedColor
        z: 1

        Connections {
            target: root.model
            ignoreUnknownSignals: true
            function onLayoutChanged() {
                tree.childCount = root.model ? root.model.rowCount(tree.parentIndex) : 0;
            }
        }
    }

    Loader {
        id: highlightLoader
        sourceComponent: highlight

        width: root.width
        height: root.rowHeight
        z: 0
        visible: root.selectionEnabled && tree.currentItem !== null

        Binding {
            target: highlightLoader.item
            property: "y"
            value: tree.currentItem ? tree.currentItem.mapToItem(tree, 0, 0).y + tree.anchors.topMargin : 0
            when: highlightLoader.status === Loader.Ready
        }
    }
}
