import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import "../controls"

Item {
    id: evalTab

    function checkDropdownsSelected() {
        if (partSelection.selectedOption && nodeSelection.selectedOption) {
            backend.evaluation.update(partSelection.selectedOption,
                                      nodeSelection.selectedOption)
        }
    }

    YesNoPopup {
        id: saveConfigPopup
        anchors.centerIn: Overlay.overlay

        onClickedYes: backend?.evaluation.save || function () {}
    }

    RowLayout {
        id: firstRow
        height: 48
        width: parent.width
        z: 9999

        Row {
            spacing: 16

            CustomDropdown {
                id: partSelection
                defaultWidth: 150

                dropDownName: qsTr("Part")

                enabled: options.length
                options: []

                onOptionUpdated: evalTab.checkDropdownsSelected()
            }

            CustomDropdown {
                id: nodeSelection
                defaultWidth: 150

                dropDownName: qsTr("Node")

                enabled: options.length
                options: []

                onOptionUpdated: evalTab.checkDropdownsSelected()
            }
        }

        RowLayout {
            spacing: 16

            Layout.alignment: Qt.AlignRight | Qt.AlignTop

            CustomButton {
                id: getBtn
                text: qsTr("Get")

                enabled: partSelection.selectedOption &&
                         nodeSelection.selectedOption

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    backend.evaluation.get(partSelection.selectedOption,
                                           nodeSelection.selectedOption)
                }
            }

            CustomButton {
                id: setBtn
                text: qsTr("Set")

                enabled: partSelection.selectedOption &&
                         nodeSelection.selectedOption

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    backend.evaluation.set(partSelection.selectedOption,
                                           nodeSelection.selectedOption)
                }
            }

            CustomButton {
                id: saveBtn
                text: qsTr("Save")

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    saveConfigPopup.open()
                }
            }
        }
    }

    RowLayout {
        id: secondRow
        height: 40
        width: 560

        anchors {
            top: firstRow.bottom
            topMargin: 16
        }

        Label {
            id: testSetPathLabel
            text: qsTr("Test set path:")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 400
                pixelSize: 14
            }

            Layout.alignment: Qt.AlignVCenter
        }

        CustomPathInput {
            id: setPathInput

            fontMetrics: FontMetrics {
                font.pixelSize: 14
                font.family: "Inter"
                font.weight: 400
            }

            Layout.alignment: Qt.AlignRight
        }
    }

    RowLayout {
        id: thirdRow
        height: 40
        width: 560

        anchors {
            top: secondRow.bottom
            topMargin: 8
        }

        Label {
            id: resultsPathLabel
            text: qsTr("Results path:")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 400
                pixelSize: 14
            }

            Layout.alignment: Qt.AlignVCenter
        }

        CustomPathInput {
            id: resultsPathInput

            fontMetrics: FontMetrics {
                font.pixelSize: 14
                font.family: "Inter"
                font.weight: 400
            }

            Layout.alignment: Qt.AlignRight
        }
    }

    ParamsTable {
        id: paramsTable

        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            rightMargin: 120
            bottomMargin: 31
            topMargin: 185
            fill: parent
        }

        paramsModel: backend?.evaluation || null
        onClickedCopy: backend?.evaluation.copy || function() {}
    }

    CustomButton {
        id: evaluateBtn
        text: qsTr("Evaluate")

        anchors {
            right: parent.right
            bottom: parent.bottom
            bottomMargin: 83
        }

        enabled: partSelection.selectedOption && nodeSelection.selectedOption && setPathInput.text && resultsPathInput.text

        enabledColor: "#00234D"

        btnWidth: 80
        btnHeight: 35
        btnRadius: 4

        font {
            family: "Inter"
            weight: 400
            pixelSize: 12
        }

        Layout.alignment: Qt.AlignVCenter

        onClicked: {
            forceActiveFocus()
            backend.evaluation.evaluate(partSelection.selectedOption,
                                        nodeSelection.selectedOption,
                                        setPathInput.text,
                                        resultsPathInput.text)
        }
    }

    CustomButton {
        id: exportBtn
        text: qsTr("Export")

        anchors {
            right: parent.right
            bottom: parent.bottom
            bottomMargin: 32
        }

        enabledColor: "#00234D"

        btnWidth: 80
        btnHeight: 35
        btnRadius: 4

        font {
            family: "Inter"
            weight: 400
            pixelSize: 12
        }

        Layout.alignment: Qt.AlignVCenter

        onClicked: {
            forceActiveFocus()
            fileDialog.open()
        }
    }

    FileDialog {
        id: fileDialog
        fileMode: FileDialog.SaveFile

        onAccepted: {
            let path = selectedFile.toString()
            path = path.replace(/^(file:\/{3})/,"/")
            const cleanPath = decodeURIComponent(path)

            backend.evaluation.export(cleanPath)
        }
    }

    Connections {
        target: backend?.evaluation || null

        function onPartsUpdated() {
            partSelection.options = backend.evaluation.parts
        }

        function onNodesUpdated() {
            nodeSelection.options = backend.evaluation.nodes
        }
    }
}