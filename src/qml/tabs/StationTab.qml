import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import "../controls"

Item {
    id: stationTab

    RowLayout {
        id: firstRow
        width: parent.width
        height: 48
        z: 9999

        anchors {
            top: parent.top
        }

        RowLayout {
            width: 301
            spacing: 40

            Layout.alignment: Qt.AlignLeft

            Label {
                text: qsTr("Station:")
                color: "#F3F3F3"
                style: Text.Normal

                font {
                    family: "Inter"
                    weight: 500
                    pixelSize: 24
                }

                Layout.alignment: Qt.AlignVCenter
            }

            CustomDropdown {
                id: stationSelection
                defaultWidth: 170

                dropDownName: qsTr("Select Station")

                options: backend?.settings.stations || []
            }
        }

        RowLayout {
            width: 336
            spacing: 16

            Layout.alignment: Qt.AlignRight

            CustomButton {
                id: connectBtn
                text: qsTr("Connect")

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex === 3

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    forceActiveFocus()
                    const stationAddress = stationSelection.selectedOption.address
                    const stationPort = stationSelection.selectedOption.port
                    backend.station.connect_to_station(stationAddress, stationPort)
                }
            }

            CustomButton {
                id: disconnectBtn
                text: qsTr("Disconnect")

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex !== 3

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    forceActiveFocus()
                    backend.station.disconnect_from_station()
                }
            }
        }
    }

    RowLayout {
        id: secondRow
        height: 48
        width: parent.width

        anchors {
            top: firstRow.bottom
            topMargin: 32
        }

        Label {
            text: qsTr("Station Configuration:")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 500
                pixelSize: 24
            }
            Layout.alignment: Qt.AlignVCenter || Qt.AlignLeft
        }

        RowLayout {
            width: 344
            spacing: 16

            Layout.alignment: Qt.AlignRight

            CustomButton {
                id: downloadBtn
                text: qsTr("Download")

                enabledColor: "#00234D"

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex !== 3

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignVCenter

                onClicked: {
                    forceActiveFocus()
                    backend.station.download_config()
                }
            }

            CustomButton {
                id: uploadBtn
                text: qsTr("Upload")

                enabledColor: "#00234D"

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex !== 3 && mainWindow.loadIsDone

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignVCenter

                onClicked: {
                    forceActiveFocus()
                    backend.station.upload_config()
                }
            }
        }
    }

    RowLayout {
        id: thirdRow
        height: 48
        width: parent.width

        anchors {
            top: secondRow.bottom
            topMargin: 32
        }

        Layout.preferredWidth: 696

        RowLayout {
            spacing: 40

            Layout.alignment: Qt.AlignLeft

            Label {
                text: qsTr("Configuration Set:")
                color: "#F3F3F3"
                style: Text.Normal

                Layout.preferredWidth: 252

                font {
                    family: "Inter"
                    weight: 500
                    pixelSize: 24
                }
                Layout.alignment: Qt.AlignVCenter || Qt.AlignLeft
            }

            Label {
                id: confSetPath
                text: qsTr("")
                color: "#F3F3F3"
                style: Text.Normal
                wrapMode: Text.WordWrap
                maximumLineCount: 2
                elide: Text.ElideRight

                Layout.preferredWidth: 404

                font {
                    family: "Inter"
                    weight: 500
                    pixelSize: 14
                }

                CustomToolTip {
                    id: tooltip
                }

                MouseArea {
                    id: valueMouseArea
                    anchors.fill: parent
                    hoverEnabled: true

                    onEntered: {
                        if (confSetPath.truncated) {
                            tooltip.show(confSetPath.text)
                        }
                    }
                    onExited: {
                        tooltip.hide()
                    }
                }

                Layout.alignment: Qt.AlignVCenter || Qt.AlignRight
            }
        }

        RowLayout {
            spacing: 16
            Layout.alignment: Qt.AlignRight

            CustomButton {
                id: importBtn
                text: qsTr("Import")

                enabledColor: "#00234D"

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex !== 3

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignVCenter || Qt.AlignRight

                onClicked: {
                    forceActiveFocus()
                    fileDialog.open()
                }
            }

            CustomButton {
                id: exportBtn
                text: qsTr("Export")

                enabledColor: "#00234D"

                btnWidth: 80
                btnHeight: 35
                btnRadius: 4

                enabled: stationSelection.selectedOption && indicatorsList.currentIndex !== 3 && mainWindow.loadIsDone

                font {
                    family: "Inter"
                    weight: 400
                    pixelSize: 12
                }

                Layout.alignment: Qt.AlignVCenter || Qt.AlignRight

                onClicked: {
                    folderDialog.open()
                }
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Dudidudidudidudi"
        nameFilters: [ "Zip file (*.zip)" ]

        onAccepted: {
            let path = selectedFile.toString()
            path = path.replace(/^(file:\/{3})/,"/")
            const cleanPath = decodeURIComponent(path)

            confSetPath.text = cleanPath
            backend.station.import_config(cleanPath)
        }
    }

    FolderDialog {
        id: folderDialog

        onAccepted: {
            let path = currentFolder.toString()
            path = path.replace(/^(file:\/{3})/,"/")
            const cleanPath = decodeURIComponent(path)

            backend.station.export_config(cleanPath)
        }
    }
}