import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import "../controls"

Item {
    id: confTab

    function checkDropdownsSelected() {
        if (partSelection.selectedOption && nodeSelection.selectedOption && deviceSelection.selectedOption) {
            backend.configuration.update(partSelection.selectedOption,
                                         nodeSelection.selectedOption,
                                         deviceSelection.selectedOption)
        }
    }

    YesNoPopup {
        id: saveConfigPopup
        anchors.centerIn: Overlay.overlay

        onClickedYes: backend?.configuration.save || function () {}
    }

    RowLayout {
        id: firstRow
        height: 48
        width: parent.width
        z: 9999

        Row {
            spacing: 16

            CustomDropdown {
                id: partSelection
                defaultWidth: 150

                dropDownName: qsTr("Part")

                enabled: options.length
                options: []

                onOptionUpdated: confTab.checkDropdownsSelected()
            }

            CustomDropdown {
                id: nodeSelection
                defaultWidth: 150

                dropDownName: qsTr("Node")

                enabled: options.length
                options: []

                onOptionUpdated: confTab.checkDropdownsSelected()
            }

            CustomDropdown {
                id: deviceSelection
                defaultWidth: 150

                dropDownName: qsTr("Device")

                enabled: options.length
                options: []

                onOptionUpdated: confTab.checkDropdownsSelected()
            }
        }

        RowLayout {
            spacing: 16

            Layout.alignment: Qt.AlignRight | Qt.AlignTop

            CustomButton {
                id: getBtn
                text: qsTr("Get")

                enabled: partSelection.selectedOption &&
                         nodeSelection.selectedOption &&
                         deviceSelection.selectedOption

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    backend.configuration.get(partSelection.selectedOption,
                                              nodeSelection.selectedOption,
                                              deviceSelection.selectedOption)
                }
            }

            CustomButton {
                id: setBtn
                text: qsTr("Set")

                enabled: partSelection.selectedOption &&
                         nodeSelection.selectedOption &&
                         deviceSelection.selectedOption

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    backend.configuration.set(partSelection.selectedOption,
                                              nodeSelection.selectedOption,
                                              deviceSelection.selectedOption)
                }
            }

            CustomButton {
                id: saveBtn
                text: qsTr("Save")

                btnWidth: 120

                font {
                    family: "Inter"
                    weight: 700
                    pixelSize: 14
                }

                onClicked: {
                    saveConfigPopup.open()
                }
            }
        }
    }
    ParamsTable {
        id: paramsTable

        anchors {
            top: parent.top
            bottom: parent.bottom
            right: parent.right
            rightMargin: 120
            bottomMargin: 31
            topMargin: 80
            fill: parent
        }

        paramsModel: backend?.configuration || null
        onClickedCopy: backend?.configuration.copy || function() {}
    }

    CustomButton {
        id: exportBtn
        text: qsTr("Export")

        anchors {
            right: parent.right
            bottom: parent.bottom
            bottomMargin: 32
        }

        enabledColor: "#00234D"

        btnWidth: 80
        btnHeight: 35
        btnRadius: 4

        font {
            family: "Inter"
            weight: 400
            pixelSize: 12
        }

        Layout.alignment: Qt.AlignVCenter

        onClicked: {
            forceActiveFocus()
            fileDialog.open()
        }
    }

    FileDialog {
        id: fileDialog
        fileMode: FileDialog.SaveFile

        onAccepted: {
            let path = selectedFile.toString()
            path = path.replace(/^(file:\/{3})/,"/")
            const cleanPath = decodeURIComponent(path)

            backend.configuration.export(cleanPath)
        }
    }

    Connections {
        target: backend?.configuration || null

        function onPartsUpdated() {
            partSelection.options = backend.configuration.parts
        }

        function onNodesUpdated() {
            nodeSelection.options = backend.configuration.nodes
        }

        function onDevicesUpdated() {
            deviceSelection.options = backend.configuration.devices
        }
    }
}