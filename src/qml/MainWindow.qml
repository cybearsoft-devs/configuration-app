import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import "controls"
import "tabs"


ApplicationWindow {
    id: mainWindow
    color: "#363636"
    visible: true
    minimumWidth: 1280
    minimumHeight: 720

    title: "Configuration Tool"

    property QtObject backend

    property bool loadIsDone: false

    property bool taskRunning: false

    signal hideTooltips()

    Spinner {
        id: spinner
        z: 9999
        running: mainWindow.taskRunning

        anchors.centerIn: parent
    }

    Rectangle {
        id: overlay
        color: "#000000"
        opacity: 0.6
        visible: spinner.running
        z: 9998

        anchors.fill: parent

        MouseArea {
            anchors.fill: parent
            preventStealing: true
            hoverEnabled: true
            onClicked: {}
            onWheel: {}
            onPressed: {}
            onReleased: {}
            onDoubleClicked: {}
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            forceActiveFocus()
            hideTooltips()
        }
    }

    ConnectionStatusPopup {
        id: connectedPopup
        x: mainWindow.width - connectedPopup.width - 40
        y: -12

        icon: qsTr("../../../resources/themes/material/icons/connected_icon.svg")
        backgroundColor: "#208129"
        text: "Connected successfully"
    }

    ConnectionStatusPopup {
        id: notConnectedPopup
        x: mainWindow.width - notConnectedPopup.width - 40
        y: -12

        icon: qsTr("../../../resources/themes/material/icons/not_connected_icon.svg")
        backgroundColor: "#FF0000"
        text: "No connection"
    }

    header: ToolBar {
        id: toolBar
        width: parent.width
        height: 50
        background: Rectangle {
            implicitHeight: 50
            color: "#434343"
        }

        Rectangle {
            id: appIcon

            anchors {
                left: parent.left
                leftMargin: 14
                verticalCenter: parent.verticalCenter
            }

            width: 35
            height: 35
            color: "transparent"

            Image {
                anchors.fill: parent
                fillMode: Image.PreserveAspectFit
                source: ""
            }
        }

        Label {
            id: stationBtnLabel
            text: qsTr("")
            color: "#F3F3F3"
            style: Text.Normal

            font {
                family: "Inter"
                weight: 700
                pixelSize: 16
                capitalization: Font.AllUppercase
            }

            anchors {
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
            }
        }

         RowLayout {
            z: 2

            anchors {
                left: separator.left
                right: separator.right
                bottom: separator.top
                bottomMargin: -8
            }
            CustomTabBar {
                id: bar
                width: 464
                height: 24
                spacing: 40

                Layout.alignment: Qt.AlignLeft
            }

            ListView {
                id: indicatorsList
                width: 336
                height: 24
                spacing: 16
                orientation: ListView.Horizontal

                currentIndex: 3

                model: [{text: "Error",   activeColor: "#FF0000", index: 0},
                        {text: "OffLine", activeColor: "#FFF500", index: 1},
                        {text: "Online",  activeColor: "#14FF00", index: 2},
                        {text: "Off",     activeColor: "#959595", index: 3}]

                delegate: ConnectionStatusIndicator {
                    text: modelData.text
                    index: modelData.index
                    activeColor: modelData.activeColor
                }

                Layout.alignment: Qt.AlignRight
            }
         }

        ToolSeparator {
            id: separator
            width: parent.width
            orientation: Qt.Horizontal

            contentItem: Rectangle {
                implicitHeight: 1
                color: "#5A5A5A"
            }

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
                topMargin: 105
                leftMargin: 40
                rightMargin: 40
            }
        }
    }

    StackLayout {
        currentIndex: bar.currentIndex

        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            leftMargin: 40
            rightMargin: 40
            topMargin: 130
            fill: parent
        }

        StationTab {
            id: stationTab
        }
        ConfigurationTab {
            id: configurationTab
        }
        EvaluationTab {
            id: evaluationTab
        }
    }

    Connections {
        target: backend || null

        function onShowSpinner() {
            mainWindow.taskRunning = true
        }

        function onHideSpinner() {
            mainWindow.taskRunning = false
        }
    }

    Connections {
        target: backend?.station || null

        function onConnectionStatusUpdated() {
            indicatorsList.currentIndex = backend.station.conn_status
        }

        function onConnected() {
            notConnectedPopup.close()
            connectedPopup.open()
        }

        function onNotConnected() {
            connectedPopup.close()
            notConnectedPopup.open()
        }

        function onDownloadConfigCompleted() {
            mainWindow.loadIsDone = true
        }

        function onImportConfigCompleted() {
            mainWindow.loadIsDone = true
        }
    }
}